# README #

Spart Bank é um aplicativo desenvolvido em Kotlin para consumir a api do banco PJ Bank.



### Quais tecnologias são utilizadas? ###

* Kotlin
* MVP com Clean Architeture
* Dagger para injeção de dependência
* Espresso para testes de View
* JUnit e Mockito para Testes Unitários
