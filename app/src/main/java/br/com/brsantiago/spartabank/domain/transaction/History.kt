package br.com.brsantiago.spartabank.domain.transaction

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.Date

/**
 * Created by Bruno Santiago on 20/02/2018..
 * email: bruno.santhiago@outlook.com
 */
class History {

    @SerializedName("nome")
    @Expose
    val name: String? = null

    @SerializedName("email")
    @Expose
    val email: String? = null

    @SerializedName("cpf")
    @Expose
    val doc: String? = null

    @SerializedName("notificado_em")
    @Expose
    val notificationDate: Date? = null

    @SerializedName("status_decisao")
    @Expose
    val decisionStatus: String? = null

    @SerializedName("data_decisao")
    @Expose
    val decisionDate: Date? = null

    @SerializedName("motivo")
    @Expose
    val reason: String? = null

    @SerializedName("imagem")
    @Expose
    val image: String? = null
}