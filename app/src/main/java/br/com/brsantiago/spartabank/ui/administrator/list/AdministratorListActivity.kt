package br.com.brsantiago.spartabank.ui.administrator.list

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import br.com.brsantiago.spartabank.R
import br.com.brsantiago.spartabank.domain.company.Administrator
import br.com.brsantiago.spartabank.ui.base.BaseActivity
import kotlinx.android.synthetic.main.administrator_list_activity.*

/**
 * Created by Bruno Santiago on 17/02/2018..
 * email: bruno.santhiago@outlook.com
 */
class AdministratorListActivity : BaseActivity<AdministratorListContract.View,
        AdministratorListPresenter>(),
        AdministratorListContract.View {


    private var adapter: AdministratorListAdapter? = null
    override var presenter: AdministratorListPresenter = AdministratorListPresenter()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.administrator_list_activity)
        initialize()
        presenter.loadAdministrators()

        toolbar.title = getString(R.string.administrators)
        showProgress()

    }
    private fun initialize() {
        adapter = AdministratorListAdapter(ArrayList<Administrator>(), {
        })
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.adapter = adapter
    }


    override fun showAdministrators(administrators: MutableList<Administrator>) {
        adapter?.addAdministrators(administrators)
        adapter?.notifyDataSetChanged()
        hideProgress()
    }

    override fun showProgress() {
        recyclerView.visibility = View.GONE
        progress_bar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        recyclerView.visibility = View.VISIBLE
        progress_bar.visibility = View.GONE

    }
    override fun showError(error: String?) {
        super.showError(error)
        progress_bar.visibility = View.GONE
    }
}