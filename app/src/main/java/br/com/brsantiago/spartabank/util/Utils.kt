package br.com.brsantiago.spartabank.util

import br.com.brsantiago.spartabank.R
import com.squareup.moshi.Moshi

/**
 * Created by Bruno Santiago on 20/02/2018..
 * email: bruno.santhiago@outlook.com
 */
open class Utils {
    companion object {
        fun formatStatusColor(value: String ) : Int {
            if (value == "ativo") {
                return R.color.green
            }
            return R.color.red
        }
    }
}