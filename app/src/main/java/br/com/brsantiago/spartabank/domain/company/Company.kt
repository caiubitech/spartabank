package br.com.brsantiago.spartabank.domain.company

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Bruno Santiago on 20/02/2018..
 * email: bruno.santhiago@outlook.com
 */
class Company {

    @SerializedName("nome")
    @Expose
    val name: String? = null

    @SerializedName("cnpj")
    @Expose
    val doc: String? = null

    @SerializedName("cep")
    @Expose
    val zipCode: String? = null

    @SerializedName("endereco")
    @Expose
    val address: String? = null

    @SerializedName("numero")
    @Expose
    val number: String? = null

    @SerializedName("bairro")
    @Expose
    val district: String? = null

    @SerializedName("complemento")
    @Expose
    val complement: String? = null

    @SerializedName("cidade")
    @Expose
    val city: String? = null

    @SerializedName("estado")
    @Expose
    val state: String? = null

    @SerializedName("ddd")
    @Expose
    val phoneCode: String? = null

    @SerializedName("telefone")
    @Expose
    val phone: String? = null

    @Expose
    val email: String? = null

    @Expose
    val webhook: String? = null

    @SerializedName("credencial")
    @Expose
    val credential: String? = null

    @SerializedName("chave")
    @Expose
    val key: String? = null

    @Expose
    val status: String? = null
}