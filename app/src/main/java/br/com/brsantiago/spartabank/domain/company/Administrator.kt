package br.com.brsantiago.spartabank.domain.company

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Bruno Santiago on 20/02/2018..
 * email: bruno.santhiago@outlook.com
 */
class Administrator {

    @SerializedName("nome")
    @Expose
    val name: String? = null

    @SerializedName("socio")
    @Expose
    val businessPartner: Boolean? = null

    @SerializedName("imagem")
    @Expose
    val image: String? = null

    @Expose
    val email: String? = null

    @Expose
    val status: String = "ativo"
}