package br.com.brsantiago.spartabank.extension

import android.widget.ImageView
import br.com.brsantiago.spartabank.R
import br.com.brsantiago.spartabank.util.CircleTransform
import com.squareup.moshi.Moshi
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Bruno Santiago on 20/02/2018..
 * email: bruno.santhiago@outlook.com
 */

fun String.formatColor(): Int {

    if (this == "ativo")
        return R.color.green

    return R.color.red
}


fun String.toDate(): Date {
    val formatoBrasileiro = SimpleDateFormat("dd/MM/yyyy")
    return formatoBrasileiro.parse(this)
}

fun ImageView.fromPicasso(image: String?) {
    image?.let {

        Picasso.with(context)
                .load(image)
                .transform(CircleTransform())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(this)

    }
}


inline fun <reified T> Moshi.fromJson(json: String): T = this.adapter(T::class.java).fromJson(json)