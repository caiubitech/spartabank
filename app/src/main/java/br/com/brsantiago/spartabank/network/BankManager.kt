package br.com.brsantiago.spartabank.network

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Bruno Santiago on 20/02/2018..
 * email: bruno.santhiago@outlook.com
 */
class BankManager {
    private val SERVER: String = "https://api.pjbank.com.br/contadigital/"

    fun init() {
    }

    val retrofit = Retrofit.Builder()
            .baseUrl(SERVER)
            .client(OkHttpClient.Builder().build())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .build()

    fun loadAdministrators() = retrofit.create(AdministratorService::class.java)

}