package br.com.brsantiago.spartabank.ui.administrator.list

import br.com.brsantiago.spartabank.domain.company.Administrator
import br.com.brsantiago.spartabank.ui.base.BaseView
import br.com.brsantiago.spartabank.ui.base.BasePresenter

/**
 * Created by Bruno Santiago on 20/02/2018..
 * email: bruno.santhiago@outlook.com
 */

interface AdministratorListContract {

    interface View : BaseView {
        fun showAdministrators(administrators: MutableList<Administrator>)
    }

    interface Presenter : BasePresenter<View> {
        fun loadAdministrators()

    }
}