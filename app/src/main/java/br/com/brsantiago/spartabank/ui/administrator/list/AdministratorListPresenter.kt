package br.com.brsantiago.spartabank.ui.administrator.list

import br.com.brsantiago.spartabank.network.BankManager
import br.com.brsantiago.spartabank.network.error.ErrorHandler
import br.com.brsantiago.spartabank.ui.base.BasePresenterImpl
import rx.android.schedulers.AndroidSchedulers
import rx.functions.Action1
import rx.schedulers.Schedulers

/**
 * Created by Bruno Santiago on 20/02/2018..
 * email: bruno.santhiago@outlook.com
 */
class AdministratorListPresenter : BasePresenterImpl<AdministratorListContract.View>(), AdministratorListContract.Presenter {
    override fun loadAdministrators() {
        BankManager().loadAdministrators().list()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError { view?.showError(it.toString()) }
                .subscribe(Action1 { view?.showAdministrators(it) },
                        ErrorHandler(view, true, { throwable, _, _ -> view?.showError(throwable.message) }))
    }
}