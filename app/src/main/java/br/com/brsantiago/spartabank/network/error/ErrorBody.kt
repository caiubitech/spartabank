package br.com.brsantiago.spartabank.network.error


import br.com.brsantiago.spartabank.extension.fromJson
import com.squareup.moshi.Json
import com.squareup.moshi.Moshi
import retrofit2.Response
import java.io.IOException

/**
 * Created by Bruno Santiago on 20/02/2018..
 * email: bruno.santhiago@outlook.com
 */

data class ErrorBody(val code: Int,
                     @Json(name = "error") private val message: String) {
    override fun toString(): String = "{code: $code, message: \"$message\"}"

    companion object {
        val ERROR = 0
        private val moshi = Moshi.Builder().build()

        fun parseError(response: Response<*>?): ErrorBody? {
            return (response?.errorBody())?.let {
                try {
                    moshi.fromJson(it.string())
                } catch (ignored: IOException) {
                    null
                }
            }
        }
    }
}