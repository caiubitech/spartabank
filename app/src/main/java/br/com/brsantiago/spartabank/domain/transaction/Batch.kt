package br.com.brsantiago.spartabank.domain.transaction

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.math.BigDecimal
import java.util.*

/**
 * Created by Bruno Santiago on 20/02/2018..
 * email: bruno.santhiago@outlook.com
 */
class Batch {
    @SerializedName("cpf")
    @Expose
    val dueDate: Date? = null

    @SerializedName("cpf")
    @Expose
    val amountDate: Date? = null

    @SerializedName("cpf")
    @Expose
    val amount: BigDecimal? = null

    @SerializedName("cpf")
    @Expose
    val barCode: String? = null

    @SerializedName("cpf")
    @Expose
    val bankCode: String? = null

    @SerializedName("cpf")
    @Expose
    val agency: String? = null

    @SerializedName("cpf")
    @Expose
    val account: String? = null

    @SerializedName("cpf")
    @Expose
    val document: String? = null

    @SerializedName("cpf")
    @Expose
    val name: String? = null

    @SerializedName("cpf")
    @Expose
    val description: String? = null

    @SerializedName("cpf")
    @Expose
    val requester: String? = null

    @SerializedName("cpf")
    @Expose
    val accountType: String? = null

    @SerializedName("cpf")
    @Expose
    val subAccount: String? =  null
}