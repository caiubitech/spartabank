package br.com.brsantiago.spartabank.domain.transaction

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Bruno Santiago on 20/02/2018..
 * email: bruno.santhiago@outlook.com
 */
class Operation {

    @SerializedName("id_operacao")
    @Expose
    val id: String? = null

    @SerializedName("status_operacao")
    @Expose
    val status: String? = null

    @SerializedName("autorizacoes_realizadas")
    @Expose
    val made: String? = null

    @SerializedName("autorizacoes_necessarias")
    @Expose
    val needed: String? = null

    @SerializedName("msg")
    @Expose
    val message: String? = null

    @SerializedName("detalhes")
    @Expose
    val detail: String? = null

    @SerializedName("historico")
    @Expose
    val history: List<History>? = null
}