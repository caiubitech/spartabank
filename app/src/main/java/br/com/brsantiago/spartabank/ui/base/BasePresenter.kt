package br.com.brsantiago.spartabank.ui.base

/**
 * Created by Bruno Santiago on 20/02/2018..
 * email: bruno.santhiago@outlook.com
 */
interface BasePresenter<in V : BaseView> {

    fun attachView(view: V)

    fun detachView()
}