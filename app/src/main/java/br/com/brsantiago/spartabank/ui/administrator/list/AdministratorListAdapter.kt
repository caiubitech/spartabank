package br.com.brsantiago.spartabank.ui.administrator.list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.brsantiago.spartabank.R
import br.com.brsantiago.spartabank.domain.company.Administrator
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_administrador.*

/**
 * Created by Bruno Santiago on 20/02/2018..
 * email: bruno.santhiago@outlook.com
 */
class AdministratorListAdapter(private val administrators: MutableList<Administrator>,
                               val onClick: (Administrator) -> Unit)
                            : RecyclerView.Adapter<AdministratorListAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bindData(administrators[position])
    }
    override fun getItemCount(): Int = administrators.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        return LayoutInflater.from(parent?.context)
                .inflate(R.layout.item_administrador, parent, false).let {
            ViewHolder(it, onClick)
        }
    }

    class ViewHolder(override val containerView: View,
                     val onClick: (Administrator) -> Unit)
        : RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bindData(administrador: Administrator) {
            with(administrador) {
                tv_name.text = name
                tv_email.text = email
                containerView.setOnClickListener { onClick(this) }
            }
        }
    }
    fun addAdministrators(admins: List<Administrator>) {
        administrators.clear()
        administrators.addAll(admins)
    }
}