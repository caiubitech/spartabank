package br.com.brsantiago.spartabank.network

import br.com.brsantiago.spartabank.domain.company.Administrator
import rx.Observable
import retrofit2.http.GET
import retrofit2.http.Headers

/**
 * Created by Bruno Santiago on 20/02/2018..
 * email: bruno.santhiago@outlook.com
 */
interface AdministratorService {

    @Headers(
            "X-CHAVE-CONTA: a834d47e283dd12f50a1b3a771603ae9dfd5a32c")
    @GET("eb2af021c5e2448c343965a7a80d7d090eb64164/administradores")
    fun list() : Observable<MutableList<Administrator>>
}